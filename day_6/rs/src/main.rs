
use std::env;
use std::fs;
use std::collections::HashSet;
use std::hash::Hash;
use std::iter::FromIterator;
fn main() {
    let args: Vec<String> = env::args().collect();
    let data = read_input(args.get(1).expect("Missing input file path argument"));

    println!("Part 1: counted {} yesses.", count_yesses(&data, count_yesses_in_group_anyone));
    println!("Part 2: counted {} yesses.", count_yesses(&data, count_yesses_in_group_everyone));
}

fn read_input(path: &str) -> Vec<Vec<String>> {
    fs::read_to_string(path)
        .expect("Failed to read input file")
        .split("\n\n")
        .filter(|group| group.len() != 0)
        .map(|group| group.split("\n").map(|line| String::from(line)).collect())
        .collect()
}

fn count_yesses_in_group_anyone(group: &Vec<String>) -> usize {
    let mut acc_set = HashSet::new();
    group.iter()
         .for_each(|line| line.chars().for_each(|c| {acc_set.insert(c);}));
    return acc_set.len();
}

fn count_yesses_in_group_everyone(group: &Vec<String>) -> usize {
    let mut personal_sets:Vec::<HashSet<char>> = Vec::new();
    for line in group {
        let personal_set = HashSet::from_iter(line.chars());
        personal_sets.push(personal_set);
    }
    return personal_sets
        .iter()
        .fold(personal_sets[0].clone(), |acc, s| HashSet::from_iter(s.intersection(&acc).cloned()))
        .len();
}

fn count_yesses(groups: &Vec<Vec<String>>, counter: fn(&Vec<String>) -> usize) -> usize {
    groups.iter()
          .map(|group| counter(group))
          .fold(0, |total, count| total + count)
}
