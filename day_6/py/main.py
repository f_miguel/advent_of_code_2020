from more_itertools import split_at
from functools import reduce
import sys


def main():
    count = 0
    for group in read_groups(sys.argv[1]):
        if len(group) != 0:
            count += count_group_everyone(group)
    print(count)


def count_group_anyone(entries: [str]):
    acc = set()
    for entry in entries:
        for letter in entry:
            acc.add(letter)
    return len(acc)


def count_group_everyone(entries: [str]) -> int:
    personal_sets = []
    for person in entries:
        personal_set = set()
        for yes in person:
            personal_set.add(yes)
        personal_sets.append(personal_set)
    return len(reduce(lambda acc, s: acc & s, iter(personal_sets)))


def read_groups(path: str) -> [[str]]:
    groups = map(lambda group: list(filter(lambda line: len(line) != 0,
                                           map(lambda line: line.strip(), group))),
                 split_at(iter(open(path)), lambda line: len(line.strip()) == 0))
    return list(groups)


main()
