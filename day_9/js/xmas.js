exports.Xmas = class {
    constructor(numbers, preambleSize) {
        this.numbers = numbers
        this.preambleStart = 0
        this.preambleEnd = preambleSize - 1
    }

    shifted() {
        const x = new exports.Xmas(this.numbers, this.preambleEnd + 1)
        x.preambleStart = this.preambleStart + 1
        x.preambleEnd = this.preambleEnd + 1
        return x
    }

    preamble() {
        return this.numbers.slice(this.preambleStart, this.preambleEnd + 1)
    }

}

/**
 * Returns the index of the first number or a negative number
 * if all the numbers in xmas are valid.
 */
exports.findFirstInvalid = function(xmas) {
    if (xmas.length === 0) {
        return -1
    }
    const index = xmas.preambleEnd + 1
    const pair = findPair(xmas.preamble(), xmas.numbers[index])
    if (pair.length === 0) {
        return index
    }
    return exports.findFirstInvalid(xmas.shifted())
}

exports.findInterval = function(xmas, targetSum) {
    const lim = findIntervalAux(xmas.numbers, targetSum, xmas.numbers[0] + xmas.numbers[1], 0, 1)
    return xmas.numbers.slice(lim[0], lim[1] + 1)
}

function findIntervalAux(numbers, targetSum, currentSum, i, j) {
    if (currentSum === targetSum) {
        return [i, j]
    }
    if (j + 1 >= numbers.length) {
        return []
    }
    if (currentSum < targetSum) {
        return findIntervalAux(numbers, targetSum, currentSum + numbers[j + 1], i, j + 1)
    }
    return findIntervalAux(numbers, targetSum, currentSum - numbers[i], i + 1, j)
}

function findPair(numbers, targetSum) {
    if (numbers.length === 0) {
        return []
    }
    const pivot = numbers[0]

    for (let i = 1; i < numbers.length; i++) {
        if (pivot + numbers[i] === targetSum) {
            return [pivot, numbers[i]]
        }
    }
    return findPair(numbers.slice(1), targetSum)
}
