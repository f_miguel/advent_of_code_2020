import sys
from functools import reduce
import math


def closest_after(n, step):
    return math.ceil(n / step) * step


def closests(earliest, ids):
    return list(map(lambda _id: (_id, closest_after(earliest, _id)), ids))


def read_input(path):
    data = open(path).read().split('\n')
    earliest_timestamp = int(data[0])
    bus_ids = map(int, filter(lambda id: id != 'x',
                              data[1].split(',')))
    return earliest_timestamp, list(bus_ids)


def main():
    input_data = read_input(sys.argv[1])
    earliest_timestamp = input_data[0]
    bus_ids = input_data[1]
    bus_times = closests(earliest_timestamp, bus_ids)
    earliest_bus = reduce(lambda earliest, bus_time: bus_time if bus_time[1] < earliest[1] else earliest,
                          bus_times,
                          bus_times[0])
    waiting_time = earliest_bus[1] - earliest_timestamp

    print(earliest_bus[0] * waiting_time)


if __name__ == '__main__':
    main()
