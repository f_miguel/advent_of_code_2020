package advent_of_code.day_three

import kotlin.math.round
import kotlin.math.sqrt

/**
 * Objects of this class represent the map of the island composed by empty spaces
 * amd trees
 *
 * @constructor takes a matrix representing the map where the `treeChar` represents
 * trees and the `openSquareChar` represents open spaces
 *
 * @author f.furtado
 */
class IslandMap (
    private val map: List<List<Char>>,
    private val treeChar: Char,
    private val openSquareChar: Char
) {


    val width = if (map.isEmpty()) 0 else map[0].size

    val height = map.size

    /**
     * Expands the Island map by duplicating the landscape pattern once to the right
     */
    fun expandedRight() = IslandMap(map.map { it.plus(it) }, treeChar, openSquareChar)

    /**
     * Returns an `IslandMap` with a `path` which is a list of pairs where for each
     * the first  element is the x coordinate and the second one is the y coordinate
     * on the map
     */
    fun withPath(path: List<Pair<Int, Int>>, spaceCrossedChar: Char, treeFoundChar: Char): Pair<IslandMap, Int> {

        val nDuplications = 1 + round(sqrt(1/(width / path.last().first.toDouble()))).toInt()

        val rMap = expandedRight(this, nDuplications).map.toMutableList()
                .map { it.toMutableList() }

        var encounteredTrees = 0

        path.forEach { pair ->
            if (rMap[pair.second][pair.first] == openSquareChar) {
                rMap[pair.second][pair.first] = spaceCrossedChar
            } else if (rMap[pair.second][pair.first] == treeChar) {
                rMap[pair.second][pair.first] = treeFoundChar
                encounteredTrees += 1
            }
        }

        return Pair(IslandMap(rMap, treeChar, openSquareChar), encounteredTrees)
    }


    /**
     * String representation of the Island map
     */
    override fun toString(): String {
        val sb = StringBuilder()
        for (line in map) {

            // `java.lang.ClassCastException: java.util.ArrayList incompatible
            // with java.lang.Character` thrown if line is not converted to String
            for (char in line) {
                sb.append(char)
            }
            sb.append('\n')
        }
        return sb.toString()
    }
}

/**
 * Expands the Island map by duplicating the landscape pattern `n` times
 * to the right
 */
fun expandedRight(islandMap: IslandMap, n: Int): IslandMap =
    if (n == 0) islandMap
    else expandedRight(islandMap.expandedRight(), n -1)

/**
 * Reads an Island map from a `String`
 */
fun readMap(str: String, threeChar: Char, openSquareChar: Char) : IslandMap {
    val list = ArrayList<List<Char>>()
    str.split('\n')
        .filter { it.isNotBlank() }
        .forEach { list.add(it.toList()) }

    return IslandMap(list, threeChar, openSquareChar)
}

fun makePath(islandMap: IslandMap, start: Pair<Int, Int>, slope: Pair<Int, Int>): List<Pair<Int, Int>> {
    val list = ArrayList<Pair<Int, Int>>()
    var current = start

    while (current.second < islandMap.height) {
        list.add(current)
        current = Pair(current.first + slope.first,  current.second + slope.second)
    }
    return list
}
