package advent_of_code.day_three

import java.io.File

fun main(args: Array<String>) {
    val islandMap = readMap(File("day_three/input.txt").readText(), '#', '.')
    val withPath = islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(3, 1)), 'O', 'X')

    val foundTrees = listOf(
        islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(1, 1)), '0', 'X').second,
        islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(3, 1)), '0', 'X').second,
        islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(5, 1)), '0', 'X').second,
        islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(7, 1)), '0', 'X').second,
        islandMap.withPath(makePath(islandMap, Pair(0, 0), Pair(1, 2)), '0', 'X').second
    ).map(Int::toLong)

    println("Found ${withPath.second} trees following the slope (3, 1)")
    println("Found ${foundTrees.reduce { acc, i -> acc * i} } trees in total")
}

