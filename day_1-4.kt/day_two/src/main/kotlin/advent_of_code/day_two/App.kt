
package advent_of_code.day_two

import java.io.File

fun main(args: Array<String>) {
    val lfPairs = File(args[0]).readLines()
        .filter { it.isNotBlank() }
        .map {
            val split = it.split(": ")
            Pair<String, PasswordPolicy>(split[1], parseLfPolicy(split[0])) }

    val lpPairs = lfPairs
        .map{
            val lfPol = it.second as LetterFrequencyPolicy
            Pair<String, PasswordPolicy>(
            it.first,
            LetterInPositionPolicy(lfPol.letter, lfPol.min - 1, lfPol.max - 1)) }

    println("${countValid(lfPairs)} valid passwords found according to the LF Policy.")
    println("${countValid(lpPairs)} valid passwords found according to the LP Policy")


}

private fun countValid(pairs: List<Pair<String, PasswordPolicy>>) =
    pairs.filter { pair -> pair.second.checkCompliance(pair.first)}
        .count()

private fun parseLfPolicy(str: String) : LetterFrequencyPolicy{
    val letter = str.split(' ')[1].toCharArray()[0]
    val frequency = str.split(' ')[0].split('-').map(Integer::parseInt)
    return LetterFrequencyPolicy(letter, frequency[0], frequency[1])
}

private data class LetterFrequencyPolicy(
    val letter: Char,
    val min: Int,
    val max: Int
) : PasswordPolicy {

    override fun checkCompliance(str: String) =
        str.filter { it == letter }.count() in min..max
}

private data class LetterInPositionPolicy(
    val letter: Char,
    val pos1: Int,
    val pos2: Int
) : PasswordPolicy {

    override fun checkCompliance(str: String) =
        (str[pos1] != str[pos2]) && (str[pos1] == letter || str[pos2] == letter)
}

private data class Pair<T, P>(
    val first: T,
    val second: P
)
