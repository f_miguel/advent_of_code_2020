package advent_of_code.day_four

data class Passport (
    val passportId: String,
    val birthYear: String,
    val issueYear: String ,
    val expirationYear: String,
    val height: String,
    val hairColour: String,
    val eyeColour: String,
    val countryId: String

) {
    fun hasPassportId() = passportId.isNotBlank()
    fun hasBirthYear() = birthYear.isNotBlank()
    fun hasIssueYear() = issueYear.isNotBlank()
    fun hasExpirationYear() = expirationYear.isNotBlank()
    fun hasHeight() = height.isNotBlank()
    fun hasHairColour() = hairColour.isNotBlank()
    fun hasEyeColour() = eyeColour.isNotBlank()
    fun hasCountryId() = countryId.isNotBlank()

}

fun isValid(passport: Passport) : Boolean {

    return try {

        val passportId = passport.passportId.toInt()
        val birthYear = passport.birthYear.toInt()
        val issueYear = passport.issueYear.toInt()
        val expirationYear = passport.expirationYear.toInt()

        // ignoring countryId
        passport.hasPassportId() && passport.passportId.length == 9 &&
        passport.hasBirthYear() && birthYear in 1920..2002 &&
        passport.hasIssueYear() && issueYear in 2010..2020 &&
        passport.hasExpirationYear() && expirationYear in 2020..2030 &&
        passport.hasHeight() && isValidHeight(passport.height) &&
        passport.hasHairColour() && isValidHairColour(passport.hairColour) &&
        passport.hasEyeColour() && listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
                                        .contains(passport.eyeColour)

    } catch (e: NumberFormatException) {
        false
    }
}

private fun isValidHeight(height: String): Boolean {
    return try {
        when (height.takeLast(2)) {
            "cm" -> {
                val h = height.dropLast(2).toInt()
                h in 150..193
            }

            "in" -> {
                val h = height.dropLast(2).toInt()
                h in 59..76
            }
            else -> false
        }
    } catch (e: java.lang.NumberFormatException) {
        return false
    }
}

private fun isValidHairColour(hairColour: String): Boolean {
    return try {
        hairColour.startsWith('#') &&
        hairColour.drop(1).all { it in 'a'..'f' || it.toString().toInt() in 0..9}
    } catch (e: java.lang.NumberFormatException) {
        false
    }
}

