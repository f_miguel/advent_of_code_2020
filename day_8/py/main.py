#! /usr/bin/env python3

import sys
import operation as op


def read_input(path):
    return list(
        map(lambda entry: Instruction(operation_map[entry[0]], int(entry[1])),
            map(lambda entry: entry.split(' '),
                filter(lambda entry: len(entry) != 0,
                    map(lambda entry: entry.strip(),
                        iter(open(path)))))))


def map_instructions_freq(instructions):
    m = {}
    for inst in instructions:
        if inst in m:
            m[inst] = m[inst] + 1
        else:
            m[inst] = 1
    return m


def decrement_freq(freq_map, instruction):
    if instruction not in freq_map:
        return
    freq = freq_map[instruction]
    if freq < 2:
        del freq_map[instruction]
    else:
        freq_map[instruction] = freq - 1


class Instruction:
    def __init__(self, operation, arg):
        self.operation = operation
        self.argument = arg

    def __eq__(self, other):
        if self is other:
            return True
        if other is None or not isinstance(other, Instruction):
            return False
        return self.operation == other.operation \
               and self.argument == other.argument

    def __hash__(self):
        return self.operation.__hash__() + self.argument

    def __str__(self):
        return f"{self.operation}, {self.argument}"


class Program:
    def __init__(self, instructions, auto_stop = True):
        self.instructions = instructions
        self.freq = map_instructions_freq(instructions)
        self.accumulator = 0
        self.nxt_instruction = 0
        self.auto_stop = auto_stop

    def run(self):
        while True:
            inst = self.instructions[self.nxt_instruction]
            if inst not in self.freq and self.auto_stop:
                print('Stoping beacause the instruction '
                    + f'\'{inst}\' was exausted')
                return False
            inst.operation.execute(self, inst.argument)
            decrement_freq(self.freq, inst)
            self.nxt_instruction += 1
            if self.nxt_instruction > len(self.instructions) - 1:
                print(f'The program ended by itself at line {self.nxt_instruction - 1}'
                    + f' with instruction {inst}')
                return True  # the program ends by itself


operation_map = {
    'nop': op.Nop(),
    'acc': op.Acc(),
    'jmp': op.Jmp()
}


def main():
    instructions = read_input(sys.argv[1])

    program_part_1 = Program(instructions)
    program_part_1.run()
    print(f'The accumulator\'s value is: \'{program_part_1.accumulator}\'')

    def swap(instruction):
        if instruction.operation.name == 'nop':
            return Instruction(operation_map['jmp'], instruction.argument)
        return Instruction(operation_map['nop'], instruction.argument)

    print('Trying to fix the code...')
    for index, instruction in enumerate(instructions):
        operation = instruction.operation
        if (operation.name == 'jmp' or operation.name == 'nop'):
            fixed = instructions[:]
            fixed[index] = swap(instruction)
            program_part_2 = Program(fixed)
            finishes = program_part_2.run()
            if finishes:
                print('The last execution did not go into an infinite loop!')
                print(f'The accumulator\'s value is \'{program_part_2.accumulator}\'.')
                break


main()
