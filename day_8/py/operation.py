class Operation:

    def execute(self, program, arg):
        pass


class Acc(Operation):
    def __init__(self):
        self.name = 'acc'

    def execute(self, program, arg):
        program.accumulator += arg

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Acc) or other is None:
            return False
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()


class Jmp(Operation):
    def __init__(self):
        self.name = 'jmp'

    def execute(self, program, arg):
        program.nxt_instruction = program.nxt_instruction + arg - 1

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Jmp) or other is None:
            return False
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()


class Nop(Operation):
    # does nothing
    
    def __init__(self):
        self.name = 'nop'

    def __str__(self):
        return self.name

    def __eq__(self, other):
        if self is other:
            return True
        if not isinstance(other, Nop) or other is None:
            return False
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()
