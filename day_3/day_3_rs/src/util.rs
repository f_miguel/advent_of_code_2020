use std::fs;
use crate::island_map::IMap;

pub fn read_map_from_file(path: &str) -> IMap {
    let lines: Vec<Vec<char>> = fs::read_to_string(path)
        .expect(format!("Failed to read from {}", path).as_str())
        .split('\n')
        .filter(|line| !line.trim().is_empty())
        .map(|line| line.trim().chars().collect::<Vec<char>>())
        .collect();

    let lines_len = match lines.is_empty() {
        true => 0,
        false => lines[0].len()
    };

    if !lines.iter().all(|line| line.len() == lines_len) {
        panic!("Pattern is not a matrix");
    }
    IMap::new(lines)
}