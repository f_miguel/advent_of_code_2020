mod util;
mod island_map;

use util::read_map_from_file;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let map = read_map_from_file(args[1].as_str());
    let slopes: Vec<(usize, usize)> = vec![
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ];
    let r = slopes
        .iter()
        .map(|slope| map.traverse_count((0,0), *slope, '#'))
        .fold(1, |acc, n_trees| acc * n_trees);
    println!("{}", r);
}
