import sys
from functools import reduce


def read_input(path):
    return [list(line) for line in open(path).read().split('\n')
            if len(line) != 0]


def path_to_botton(matrix, slope, line, col, path):
    next_l = line + slope[0]
    next_c = ((col + slope[1]) % len(matrix[0]))
    if next_l >= len(matrix): # reached the botton
        return path
    path.append(matrix[next_l][next_c])
    return path_to_botton(matrix, slope, next_l, next_c, path)


def main():
    island_map = read_input(sys.argv[1])
    path = path_to_botton(island_map, (1, 3), 0, 0, [])
    n_trees = path.count('#')
    print(f'Part_1: found {n_trees} trees')

    # part 2
    slopes = [
        (1, 1), 
        (1, 3),
        (1, 5),
        (1, 7), 
        (2, 1)
    ]
    each_n_trees = map(lambda slope: path_to_botton(island_map, slope, 0, 0, []).count('#'),
                       slopes)
    res = reduce(lambda acc, x: acc * x, each_n_trees)
    print(f'Part_2: {res}')


if __name__ == '__main__':
    main()