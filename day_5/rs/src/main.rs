mod seat;

use seat::Seat;
use std::env;
use std::fs;
use std::collections::HashMap;

fn main() {
    let args:Vec<String> = env::args().collect();
    let input_seats: Vec<Seat> = fs::read_to_string(&args[1])
        .expect("Unable to read file!")
        .split('\n')
        .map(|entry| entry.trim())
        .filter(|entry| !entry.is_empty())
        .map(&Seat::from)
        .collect();
    let mut input_seats_map = HashMap::new();
    input_seats.iter().for_each(|seat| {input_seats_map.insert(seat.id, seat);});

        
    println!("{} is the largest seat id", input_seats.iter().fold(0, |largest, seat| if seat.id > largest {seat.id} else {largest}));
    let mut missing: Vec<Seat> = Vec::new();

    let all: Vec<Seat> = all_seats();
    all.iter().for_each(|seat| if !input_seats_map.contains_key(&seat.id) { println!("{}", seat)});

    println!("We found {}, of {} total seats", input_seats.len(), all.len());

}

fn all_seats() -> Vec<Seat> {
    let mut all_seats: Vec<Seat> = Vec::new();
    (0..128).for_each(|row| {
        (0..8).for_each(|col| all_seats.push(Seat::from_pos(row, col)))
    });
    return all_seats;
}

