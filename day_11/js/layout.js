/**
 * Returns a stabilized layout
 * @param {[[String]]} layout a matrix of characters representing the seats
 * @param {number} tolerance the number of nearby occupied seats it takes for an
 *                      occupied seat to become empty
 * @param {([[String]], number, number) => [String]} relevantSeatsGetter a function 
 *         which takes a layout, the index of a line and of a column and returns
 *         the relevant nearby seats.
 */
function stabilize(layout, tolerance, relevantSeatsGetter) {
    const candidate = applyRules(layout, tolerance, relevantSeatsGetter);
    if (equal(candidate, layout)) {
        return layout;
    }
    return stabilize(candidate, tolerance, relevantSeatsGetter);
}

// O(n)
function applyRules(layout, tolerance, relevantSeatsGetter) {
    const lay = [];
    for (let i = 0; i < layout.length; i++) {
        lay.push(layout[i].slice());
    }

    for (let i = 0; i < layout.length; i++) {
        for (let j = 0; j < layout[0].length; j++) {
            const pos = layout[i][j];
            const adjacents = relevantSeatsGetter(layout, i, j);
            if (pos === 'L' && adjacents.filter(x => x === '#').length === 0) {
                lay[i][j] = '#';
            } else if (pos === '#' && adjacents.filter(x => x === '#').length >= tolerance) {
                lay[i][j] = 'L';
            } else {
                lay[i][j] = pos;
            }
        }
    }
    return lay;
}

function getAdjacents(layout, line, col) {
    let adjacents = [];
    try {
        adjacents.push(layout[line - 1][col]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line + 1][col]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line][col - 1]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line][col + 1]);
    } catch(Exception) {}

    // diagonals
    try {
        adjacents.push(layout[line - 1][col - 1]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line + 1][col - 1]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line + 1][col + 1]);
    } catch(Exception) {}
    try {
        adjacents.push(layout[line - 1][col + 1]);
    } catch(Exception) {}
    return adjacents.filter(line => line !== undefined);
}

function getNearests(layout, line, col) {
    let adjacents = [];

    try {
        let nextUp = [line - 1, col]
        while(!(layout[nextUp[0]][nextUp[1]] !== '.' || layout[nextUp[0]][nextUp[1]] === undefined)) {
            nextUp = [nextUp[0] - 1, col]
        }
        adjacents.push(layout[nextUp[0]][col]);
    } catch(Exception) {}

    try {
        let nextDown = [line + 1, col]
        while(!(layout[nextDown[0]][nextDown[1]] !== '.' || layout[nextDown[0]][nextDown[1]] === undefined)) {
            nextDown = [nextDown[0] + 1, col]
        }
        adjacents.push(layout[nextDown[0]][col]);
    } catch(Exception) {}

    try {
        let nextLeft = [line, col - 1]
        while(!(layout[nextLeft[0]][nextLeft[1]] !== '.' || layout[nextLeft[0]][nextLeft[1]] === undefined)) {
            nextLeft = [line, nextLeft[1] - 1]
        }
        adjacents.push(layout[line][nextLeft[1]]);
    } catch(Exception) {}

    try {
        let nextRight = [line, col + 1]
        while(!(layout[nextRight[0]][nextRight[1]] !== '.' || layout[nextRight[0]][nextRight[1]] === undefined)) {
            nextRight = [line, nextRight[1] + 1]
        }
        adjacents.push(layout[line][nextRight[1]]);
    } catch(Exception) {}

    // diagonals
    try {
        let nextDtl = [line - 1, col - 1]
        while(!(layout[nextDtl[0]][nextDtl[1]] !== '.' || layout[nextDtl[0]][nextDtl[1]] === undefined)) {
            nextDtl = [nextDtl[0] - 1, nextDtl[1] - 1]
        }
        adjacents.push(layout[nextDtl[0]][nextDtl[1]]);
    } catch(Exception) {}

    try {
        let nextDbl = [line + 1, col - 1]
        while(!(layout[nextDbl[0]][nextDbl[1]] !== '.' || layout[nextDbl[0]][nextDbl[1]] === undefined)) {
            nextDbl = [nextDbl[0] + 1, nextDbl[1] - 1]
        }
        adjacents.push(layout[nextDbl[0]][nextDbl[1]]);
    } catch(Exception) {}

    try {
        let nextDbr = [line + 1, col + 1]
        while(!(layout[nextDbr[0]][nextDbr[1]] !== '.' || layout[nextDbr[0]][nextDbr[1]] === undefined)) {
            nextDbr = [nextDbr[0] + 1, nextDbr[1] + 1]
        }
        adjacents.push(layout[nextDbr[0]][nextDbr[1]]);
    } catch(Exception) {}

    try {
        let nextDtr = [line - 1, col + 1]
        while(!(layout[nextDtr[0]][nextDtr[1]] !== '.' || layout[nextDtr[0]][nextDtr[1]] === undefined)) {
            nextDtr = [nextDtr[0] - 1, nextDtr[1] + 1]
        }
        adjacents.push(layout[nextDtr[0]][nextDtr[1]]);
    } catch(Exception) {}
    return adjacents.filter(line => line !== undefined);
}

/**
 * Returns true if the given layouts have the exact same sise and elements,
 * returns false otherwise.
 */
function equal(layout1, layout2) {
    if (layout1.length !== layout2.length) {
        return false;
    }
    for (let i = 0; i < layout1.length; i++) {
        if (!eqArray(layout1[i], layout2[i])) {
            return false;
        }
    }
    return true;
}

/**
 * Returns true if the two given arrays have the same size and elements.
 */
function sameElements(arr1, arr2) {
    if (arr1.length !== arr2.length) {
        return false;
    }
    for (let i = 0; i < arr1.length; i++) {
        if (!arr2.includes(arr1[i])) {
            return false;
        }
    }
    return true;
}

/**
 * Returns true if every position in arr1 contains an elements matching the
 * element at the same position in arr2, returns false otherwise.
 */
function eqArray(arr1, arr2) {
    if (arr1.length !== arr2.length) {
        return false;
    }
    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) {
            return false;
        }
    }
    return true;
}

// O(n)
function countOccupied(layout) {
    return layout.reduce((occupied, line) => {
        let lineSum = 0;
        line.forEach(pos => {
            if (pos === '#') {
                lineSum++
            }
        });
        return occupied + lineSum   ;
    }, 0)
}


module.exports = {
    getAdjacents,
    sameElements,
    equal,
    stabilize,
    applyRules,
    getNearests,
    countOccupied
};
