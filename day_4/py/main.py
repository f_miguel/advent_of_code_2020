#! /usr/bin/env python3

import re
import sys
from functools import reduce


def read_input(path):
    entries = [re.split(' |\n', entry) for entry in open(path).read().split('\n\n')
               if len(entry) != 0]
    return list(map(entry_as_map, entries))


def entry_as_map(entry):
    the_map = {}
    for field in map(lambda s: s.split(':'), entry):
        the_map[field[0]] = field[1]
    return the_map


def count_valid_passports(passports, required_fields, validator_func):
    return reduce(lambda acc, pa: acc + 1 if validator_func(pa, required_fields)
                                          else acc,
                                          passports, 0)


def is_valid_passport(passport, required_fields):
    for field in required_fields:
        if field not in passport:
            return False
    return True


def is_valid_passport_p2(passport, required_fields):
    for field in required_fields:
        if field[0] in passport:
            patt = re.compile(field[1])
            if not re.fullmatch(patt, passport[field[0]]):
                return False
        else:
            return False
    return True


def main():
    cm_patt = '(1[5-8][0-9]|19[0-3])cm'
    in_patt = '((59|6[0-9])|(7[0-6]))in'
    required_fields = [
        ('byr', '(19[2-9][0-9]|200[0-2])'),
        ('iyr', '(201[0-9]|2020)'),
        ('eyr', '(202[0-9]|2030)'),
        ('hgt', f'({cm_patt}|{in_patt})'),
        ('hcl', '#([0-9]|[a-f]){6}'),
        ('ecl', 'amb|blu|brn|gry|grn|hzl|oth'),
        ('pid', '[0-9]{9}')
    ]
    passports = read_input(sys.argv[1])
    n_valid = count_valid_passports(passports, [field[0] for field in required_fields], is_valid_passport)
    print(n_valid)

    n_valid_p2 = count_valid_passports(passports, required_fields, is_valid_passport_p2)
    print(f'part2: {n_valid_p2}')


if __name__ == '__main__':
    main()
