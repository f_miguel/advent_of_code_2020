use regex::Regex;
use std::fs;
use std::collections::HashMap;
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    let cm_patt = "(1[5-8][0-9]|19[0-3])cm";
    let in_patt = "((59|6[0-9])|(7[0-6]))in";
    let hgt_patt = format!("({}|{})", cm_patt, in_patt);

    let required_fields = vec![
        ("byr", "(19[2-9][0-9]|200[0-2])"),
        ("iyr", "(201[0-9]|2020)"),
        ("eyr", "(202[0-9]|2030)"),
        ("hgt", hgt_patt.as_str()),
        ("hcl", "#([0-9]|[a-f]){6}"),
        ("ecl", "amb|blu|brn|gry|grn|hzl|oth"),
        ("pid", "[0-9]{9}")
    ];
    let passports = read_input(args.get(1).expect("Missing input path argument"));
    println!("{}", count_valid(&required_fields.iter().map(|field| field.0).collect(), &passports));
    println!("{}", count_valid_p2(&required_fields, &passports));

}


fn read_input(path: &str) -> Vec<HashMap<String, String>> {
    fs::read_to_string(path)
        .expect(format!("Failed to read input file at {}", path).as_str())
        .split("\n\n")
        .filter(|entry| entry.len() != 0)
        .map(|entry| parse_passport(entry))
        .collect()
}

fn parse_passport(entry: &str) -> HashMap<String, String> {
    let mut passport = HashMap::new();
    Regex::new(" |(\n)").unwrap()
        .split(entry)
        .filter(|field| field.len() != 0)
        .map(|entry| entry.split_once(":").unwrap())
        .for_each(|entry| {
            passport.insert(String::from(entry.0), String::from(entry.1));
        });
    return passport;
}

fn is_valid_passport(required_fields: &Vec<&str>, passport: &HashMap<String, String>) -> bool {
    required_fields.iter().all(|field| passport.contains_key(&String::from(*field)))
}

fn count_valid(required_fields: &Vec<&str>, passports: &Vec<HashMap<String, String>>) -> usize {
    passports
        .iter()
        .fold(0, |acc, passport| {
            if is_valid_passport(required_fields, passport) {
                 acc + 1
            } else {
                acc
            }
        })
}

fn is_valid_passport_p2(required_fields: &Vec<(&str, &str)>, passport: &HashMap<String, String>) -> bool {
    required_fields.iter()
        .all(|field|
            passport.contains_key(field.0) &&
            fully_matches(passport.get(field.0).unwrap(), field.1)
        )
}

fn count_valid_p2(required_fields: &Vec<(&str, &str)>, passports: &Vec<HashMap<String, String>>) -> usize {
    passports
        .iter()
        .fold(0, |acc, passport| {
            if is_valid_passport_p2(required_fields, passport)  {
                acc + 1
            } else {
                acc
            }
        })

}

fn fully_matches(text: &str, regex: &str) -> bool {
    let re = Regex::new(regex).expect("Failed to parse regex");
    match re.find(text) {
        Some(mat) => {
            if mat.start() == 0 && mat.end() == text.len() {
                true
            } else { false }
        },
        _ => false
    }
}
