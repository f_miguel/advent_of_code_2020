#! /usr/bin/env python3

import sys
from tree import Node
import adapter as adpt
from adapter import count_leaves
from adapter import Adapter



def read_input(path):
    return list(
    map(lambda entry: Adapter(int(entry)),
        filter(lambda entry: entry.strip(),
            map(lambda entry: entry.strip(),
                iter(open(path))))))


def main():
    # part 1
    adapters = read_input(sys.argv[1])
    adapters.sort()

    # insert the outlet and the device
    adapters.insert(0, Adapter(0))
    adapters.append(Adapter(adapters[len(adapters) - 1].joltage + 3))

    adpt.connect_adapters(adapters)

    diffs = {}
    for diff in adpt.extract_diffs(adapters):
        diffs[diff] = diffs[diff] + 1 if diff in diffs else 0

    print(diffs)

    # part 2
    comb_tree = adpt.combinations_tree(adapters, Node(adapters[0]))
    print(count_leaves(comb_tree))


if __name__ == '__main__':
    main()
