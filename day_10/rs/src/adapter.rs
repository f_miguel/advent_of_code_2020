use std::cmp::Ordering;
use std::cmp::Ord;

#[derive(Debug)]
pub struct Adapter<'a> {
    joltage: u32,
    connection: Option<Box<&'a Adapter<'a>>>
}

impl<'a> Adapter<'_> {
    pub fn new(joltage: u32) -> Adapter<'a>{
        Adapter {
            joltage,
            connection: None
        }
    }

    pub fn connAdapters(adapters: &'a Vec<Adapter>) {
        for i in 1..adapters.len() {
            adapters[i].connection = Some(Box::from(&adapters[i - 1]));
        }
    }
}

impl PartialEq for Adapter<'_> {
    fn eq(&self, other: &Adapter) -> bool {
        self.joltage == other.joltage
    }
}

impl Eq for Adapter<'_> {}

impl PartialOrd for Adapter<'_> {
    fn partial_cmp(&self, other: &Adapter) -> Option<Ordering> {
        if self.joltage < other.joltage {
            return Some(Ordering::Less);
        } else if self.joltage > other.joltage {
            return Some(Ordering::Greater)
        } else {
            return Some(Ordering::Equal)
        }
    }
}

impl Ord for Adapter {
    fn cmp(&self, other: &Adapter) -> Ordering {
        if self.joltage < other.joltage {
            return Ordering::Less;
        } else if self.joltage > other.joltage {
            return Ordering::Greater;
        } else {
            return Ordering::Equal;
        }
    }
}