tree = require('./tree')

exports.Adapter = class {

    constructor(joltage) {
        this.joltage = joltage
        this.connection = null
    }

    canConnect(adapter) {
        const joltageDiff = this.joltage - adapter.joltage
        return joltageDiff > 0 && joltageDiff < 4
    }

    toString() {
        return `{ joltage: ${this.joltage}, is_connected: ${this.connection !== null} }`
    }
}

exports.connectAdapters = function(adapters) {
    for (let i = 1; i < adapters.length; i++) {
        const connector = adapters[i]
        const candidate = adapters[i - 1]
        if (connector.canConnect(candidate)) {
            connector.connection = candidate
        }
    }
}

exports.countConnDiffs = function(adapters) {
    const diffs = {}
    for (let i = 1; i < adapters.length; i++) {
        const diff = adapters[i].joltage - adapters[i - 1].joltage
        if (diffs[diff]) {
            diffs[diff] = diffs[diff] + 1
        } else {
            diffs[diff] = 1
        }
    }
    return diffs
}

exports.compareAdapters = function(adapter1, adapter2) {
    return adapter1.joltage - adapter2.joltage
}

exports.arrangementsTree = (adapters, node) => {
    return arrangementsTreeAux(adapters, node, new Map())
}

let arrangementsTreeAux = function(adapters, node, cache) {
    if (adapters.length === 0) { return }

    const pivot = node.data

    if (cache[pivot.joltage]) {
        node.addChildren(cache[pivot.joltage])
    } else {
        for (let i = 1; i < adapters.length - 1; i++) {
            if (adapters[i].canConnect(pivot)) {
                node.addChildren(new tree.Node(adapters[i]))
            } else { break }
        }
        cache[pivot.joltage] = node.children
        let i = 1
        node.children.forEach(child => {
            arrangementsTreeAux(adapters.slice(i), child, cache)
            i++
        })
    }
    return node
}

exports.countLeaves = (node) => {
    return countLeavesAux(node, new Map())
}

let countLeavesAux = function(node, cache) {
    if (node.children.length === 0) {
        return 1
    }
    if (cache[node.data.joltage]) {
        return cache[node.data.joltage]
    }
    let leavesSum = 0
    node.children.forEach(child => leavesSum += countLeavesAux(child, cache))
    cache[node.data.joltage] = leavesSum
    return leavesSum
}
