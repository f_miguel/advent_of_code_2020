use std::fs;
use std::env;


// Expects 2 command line arguments, the input file path, and number
fn main() {
    let args = env::args()
        .collect::<Vec<String>>();
    let sum: i32 = args.get(2)
                    .expect("Expected the sum as second arg!")
                    .parse()
                    .expect("Expected the second arg to be a number!");
    let entries = read_entries(
                args.get(1)
                .expect("Expected the input file path as first arg!").as_str()
    );

    let possible_pair = find_pair(&(|n1, n2| n1 + n2 == sum), &entries[0..]);
    match possible_pair {
        Some(pair) => println!("{} + {} == {}", pair.0, pair.1, sum),
        None => println!("No pair found!")
    }

    let possible_triple = find_triple(sum, &entries[0..]);
    match possible_triple {
        Some(triple) => println!("{} + {} + {} = {}", triple.0, triple.1, triple.2, sum),
        None => println!("No triple found!")
    }
}

fn find_triple(sum: i32,  vec:  &[i32]) -> Option<(i32, i32, i32)> {
    if vec.len() < 3 {
        return None;
    }

    let pivot = vec[0];
    let possible_pair = find_pair(&(|n1, n2| n1 + n2 == sum - pivot), vec);
 
    possible_pair.map_or_else(|| find_triple(sum, &vec[1..]), 
                           |pair| Some((pivot, pair.0, pair.1)))
}

fn find_pair(predicate: &dyn Fn(i32, i32) -> bool, vec: &[i32]) -> Option<(i32, i32)> {
    if vec.len() < 2 {
        return None;
    }

    let pivot = vec[0];
    for num in &vec[1..] {
        if predicate(pivot, *num) {
            return Some((pivot, *num));
        }
    }
    
    find_pair(predicate, &vec[1..])
}

fn read_entries(file_path: &str) -> Vec<i32> {
    fs::read_to_string(file_path)
        .expect("Unable to read file!")
        .split('\n')
        .filter(|str_entry| !str_entry.trim().is_empty())
        .map(|str_entry| str_entry
                            .trim()
                            .parse::<i32>()
                            .unwrap_or_else(|_| panic!("Failed to convert entry: '{}'", str_entry)))
        .collect()
}
