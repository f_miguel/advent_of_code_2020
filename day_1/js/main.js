#! /usr/bin/env node

fs = require('fs')

console.log(process.argv[2])
fs.readFile(process.argv[2], 'UTF-8', (err, data) => {
    const numbers = data.split('\n')
                      .map(line => line.trim())
                      .filter(line => line.length != 0)
                      .map(line => parseInt(line))
    console.log(find_pair(numbers, parseInt(process.argv[3])))
    console.log(find_tripple(numbers, parseInt(process.argv[3])))
})

function find_pair(numbers, target) {
    if (numbers.length === 0) {
        return []
    }
    const pivot = numbers[0]
    for (let i = 1; i < numbers.length; i++) {
        if (pivot + numbers[i] === target) {
            return [pivot, numbers[i]]
        }
    }
    return find_pair(numbers.slice(1), target)
}

function find_tripple(numbers, target) {
    if (numbers.length === 0) {
        return []
    }
    const pivot = numbers[0]
    const target_sum = target - pivot
    const pair = find_pair(numbers.slice(1), target_sum)
    if (pair.length !== 0) {
        return [pivot, pair[0], pair[1]]
    }
    return find_tripple(numbers.slice(1), target)
}

