<?php

function find_pair(array $numbers, int $target_sum): array {
    if (empty($numbers)) {
        return [];
    }
    $pivot = $numbers[0];
    $subarr = array_slice($numbers, 1);
    foreach ($subarr as $num) {
        if (($pivot + $num) == $target_sum) {
            return [$pivot, $num];
        }
    }
    return find_pair($subarr, $target_sum);
}

function find_tripple(array $numbers, int $target_sum): array {
    if (empty($numbers)) {
        return [];
    }
    $pivot = $numbers[0];
    $target = $target_sum - $pivot;
    $pair = find_pair(array_slice($numbers, 1), $target);
    if (!empty($pair)) {
        return [$pivot, ...$pair];
    }
    return find_tripple(array_slice($numbers, 1), $target_sum);
}

function read_input(string $path): array {
    return array_map(function($num) {
        return (int) $num;
    }, file($path));
}


$target = (int) $argv[2];
$nums = read_input($argv[1]);
$pair = find_pair($nums, $target);

if (empty($pair)) {
    echo "No pair summing to $target was found!\n";
} else {
    echo "$pair[0] + $pair[1] = $target\n";
}

$tripple = find_tripple($nums, $target);
if (empty($tripple)) {
    echo "No tripple summing to $target was found!\n";
} else {
    echo "$tripple[0] + $tripple[1] + $tripple[2] = $target\n";
}

?>
