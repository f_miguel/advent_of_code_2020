#! /usr/bin/env node

const fs = require('fs');
const aocMap = require('./aocMap');

function readInput(path) {
    return fs.readFileSync(path, 'UTF-8')
            .split('\n')
            .filter(line => line.length !== 0)
            .map(line => [line[0], parseInt(line.slice(1))]);
}

function main() {
    const actions = {
        'N': aocMap.moveShipNorth,
        'S': aocMap.moveShipSouth,
        'W': aocMap.moveShipWest,
        'E': aocMap.moveShipEast,
        'F': aocMap.moveShipForward,
        'L': aocMap.turnShipLeft,
        'R': aocMap.turnShipRight
    };
    const theMap = new aocMap.AocMap(new aocMap.Ship(0), 0, 0);
    const instructions = readInput('../input.txt');
    instructions.forEach(instruction => actions[instruction[0]](theMap, instruction[1]));
    console.log(`The ship's final position is <${aocMap.prettyShipCoordinates(theMap)}>`
        + ` and the manhattan distance is <${Math.abs(theMap.shipN) + Math.abs(theMap.shipE)}>`);
}

main();