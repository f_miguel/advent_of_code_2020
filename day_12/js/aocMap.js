class AocMap {
    constructor(ship, startN, startE) {
        this.ship = ship;
        this.shipN = startN;
        this.shipE = startE;
    }
}

class Ship {
    constructor(startOrientation) {
        this.orientation = startOrientation;
    }

    turn(dir, degrees) {
        if (dir === 'right') {
            this.orientation = (this.orientation + degrees) % 360;
        } else if (dir === 'left') {
            const pos = this.orientation - degrees;
            if (pos >= 0) {
                this.orientation = pos;
            } else {
                const potOrientation = this.orientation - (degrees % 360);
                this.orientation = potOrientation >= 0 ? potOrientation : 360 + potOrientation;
            }

        } else {
            throw new Error('invalid dir: ' + dir);
        }
    }

    direction() {
        if (this.orientation === 0) {
            return 'E';
        } else if (this.orientation === 90) {
            return 'S';
        } else if (this.orientation === 180) {
            return 'W';
        } else if (this.orientation === 270) {
            return 'N';
        } else {
            throw new Error('Unsupported Orientation');
        }
    }
}

function moveShipNorth(aocMap, units) {
    aocMap.shipN = aocMap.shipN + units;
}

function moveShipSouth(aocMap, units) {
    aocMap.shipN = aocMap.shipN - units;
}

function moveShipEast(aocMap, units) {
    aocMap.shipE = aocMap.shipE + units;
}

function moveShipWest(aocMap, units) {
    aocMap.shipE = aocMap.shipE - units;
}

function moveShipForward(aocMap, units) {
    if (aocMap.ship.direction() === 'N') {
        moveShipNorth(aocMap, units);
    } else if (aocMap.ship.direction() === 'S') {
        moveShipSouth(aocMap, units);
    } else if (aocMap.ship.direction() === 'W') {
        moveShipWest(aocMap, units);
    } else {
        moveShipEast(aocMap, units);
    }
}

function turnShipLeft(aocMap, units) {
    aocMap.ship.turn('left', units);
}

function turnShipRight(aocMap, units) {
    aocMap.ship.turn('right', units);
}

function prettyShipCoordinates(aocMap) {
    return `${Math.abs(aocMap.shipN)}${aocMap.shipN >= 0 ? 'N' : 'S'} `
    + `${Math.abs(aocMap.shipE)}${aocMap.shipE > 0 ? 'E' : 'W'}`
}

module.exports = {
    AocMap,
    Ship,
    moveShipNorth,
    moveShipSouth,
    moveShipWest,
    moveShipEast,
    moveShipForward,
    turnShipLeft,
    turnShipRight,
    prettyShipCoordinates
}

