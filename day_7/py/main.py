#! /usr/bin/env python3

import sys
import parsing as prs
import bag as b
from typing import Set
from functools import reduce


def count_may_contain(all_bags, target_bags, count: Set[b.Bag]):
    if len(target_bags) == 0:
        return count
    can_contain_list = []
    for t_bag in target_bags:
        for a_bag in all_bags:
            if a_bag.may_contain(t_bag) and a_bag not in count:
                count.add(a_bag)
                can_contain_list.append(a_bag)

    return count_may_contain(all_bags, can_contain_list, count)


entries = filter(lambda entry: len(entry) != 0,
                 map(lambda entry: entry.strip(),
                     iter(open(sys.argv[1]))))

for entry in entries:
    prs.parse_entry(entry)


def count_individual_bags(bag) -> int:
    count = 1
    for child_bag_entry in bag.can_contain:
        count += child_bag_entry[1] * count_individual_bags(child_bag_entry[0])
    return count



shiny_bag = b.bag_of("shiny gold")
print(len(count_may_contain(b.interned.keys(), [shiny_bag], set())))
print(count_individual_bags(shiny_bag) - 1)
