import bag as b


def parse_entry(line: str):
    """
    Makes sure all the bags in the given line are recorded and assignes the
    containable bags the first bag.
    """
    split = line.split('bags contain')
    declaration = split[0].strip()

    assignments = list(filter(lambda a: a is not None,
                         map(lambda a: parse_assignment(a), split[1].strip()
                         [0: len(split[1]) - 2].split(', '))))  # removes '.'

    bag = b.bag_of(declaration)
    for bag_assignment in assignments:
        bag.add_containable_bag(bag_assignment[0], bag_assignment[1])


def parse_assignment(assignment: str):
    """
    Returns a tupple where the first value is the color_code of the bag and
    the second is the containable number.
    """
    if assignment == 'no other bags':
        return

    n = int(assignment[0:1])
    bag = b.bag_of(assignment[2: len(assignment) - 4].strip())
    return bag, n
