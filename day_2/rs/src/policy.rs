use std::fmt;
use std::fmt::Formatter;

pub struct Policy {
    n1: usize,
    n2: usize,
    character: char
}

impl Policy {
    pub fn new(n1: usize, n2:usize, character: char) -> Policy {
        Policy {
            n1,
            n2,
            character
        }
    }

    pub fn from_string(pol: &str) -> Policy {
        let space_split: Vec<&str> = pol.split_whitespace().collect();
        let nums: Vec<usize> = space_split[0]
            .split('-')
            .map(|n| n.parse::<usize>().expect("Failed conversion"))
            .collect();
        return Policy::new(nums[0], nums[1], space_split[1].chars().collect::<Vec<char>>()[0]);
    }
}

impl fmt::Display for Policy {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Policy[n1: {}; n2: {}; char: {}]", self.n1, self.n2, self.character)
    }
}