mod policy;

use std::fs;
use std::env;

use crate::policy::Policy;

fn main() {
    let args:Vec<String> = env::args().collect();
    read_input(args[1].as_str()).iter().for_each(|entry| println!("{} ||| {}", entry.0, entry.1));
}

fn read_input(path: &str) -> Vec<(Policy, String)>{
    fs::read_to_string(path)
        .expect("Failed to read input file")
        .split('\n')
        .filter(|line| line.len() != 0)
        .map(|entry| {
            let splited: Vec<&str> = entry.split(": ").collect();
            (Policy::from_string(splited[0]), String::from(splited[1]))
        }).collect()
}


