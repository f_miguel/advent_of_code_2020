import sys


class Policy:
    def __init__(self, n1, n2, character):
        self.n1 = n1
        self.n2 = n2
        self.character = character

    @staticmethod
    def from_str(policy_str):
        split_space = policy_str.split(' ')
        nums = list(map(int, iter(split_space[0].split('-'))))
        return Policy(nums[0], nums[1], split_space[1])

    def __str__(self):
        return f'Policy[n1: {self.n1}; n2: {self.n2}; character: {self.character}]'


def read_input(path):
    return list(map(lambda entry: (Policy.from_str(entry[0]), entry[1]), 
               [entry.split(': ') for entry in open(path).read().split('\n') if len(entry) != 0]))


def complies(text, policy):
    occur = text.count(policy.character)
    return policy.n1 <= occur <= policy.n2


def complies_v2(text, policy):
    pos1 = text[policy.n1 - 1]
    pos2 = text[policy.n2 - 1]
    return (pos1 == policy.character) ^ (pos2 == policy.character)


def count_valid(entries, compliance_checker):
    # entries: [(policy, str)]
    complied = 0
    for entry in entries:
        if compliance_checker(entry[1], entry[0]):
            complied += 1
    return complied


print(count_valid(read_input(sys.argv[1]), complies_v2))
